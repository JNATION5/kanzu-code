

 **Name**
custom plugin that creates a custom post type (CPT) “Books” on
plugin activation


** Description**
What this code https://drive.google.com/file/d/19GZrDN2d5VJ6Ft_ST8kImjylX3dg57Cv/view?usp=sharingdoes is that it registers a post type 'books' with an array of arguments. These arguments are the options of our custom post type.

This array has two parts, the first part is labeled, which itself is an array. The second part contains other arguments like public visibility, has archive, slug, and show_in_rest enables block editor support.


 **Visuals**

I have shared these links for the screenshots of my project and video to guide others in following up what i have done.
screenshots> https://drive.google.com/file/d/12ujk8OMUkpXyppmGtT4Dw6JxHm92gnR7/view?usp=sharing
video> https://drive.google.com/file/d/19_Pm1hU39OKjiuI2jbV_ROQdpt_4ORan/view?usp=sharing

**Installation**
I used the system on a local wordpress installation with a help of an FTP  hosted on XAMPP server

**Usage**
I used _Vs code_ to input my functions that made the plugin which created post types.
The code is free to be copied in any other editor like web builder and run to do the same purpose.

**Roadmap**
I hope to develop a more dynamic system where by the user will be able to choose from a variety of books to read through a gutenberg block and make a purchace online and download the book or read on the web.

 **Contributing**
Since this is like an interview exam for the coming assignments, i wish to categorically say that iam open to suggestions, corrections and reviews from members on the team and any contributing ideas and collaborations are highly appreciated.

 **Authors and acknowledgment**
I will forever be grateful for being able to be part of this project and  i look forward to  whoever i will be co-operating with. i thank God for bringing me this far and different resources like wordpress support for the guidance.

 **License**
open source project regulated under terms of Wordpress.

**Project status**
To to time constrait, i was not able to add extra information to each
WordPress user where i was supposed to capture the user’s gender, date of birth and
place of birth on the edit user page under the WordPress admin dashboard.
With this iam going to keep working on it as i wait for the fedback of the first project.
